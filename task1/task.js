const tasks = [
    {id: 234, title: 'Create user registration API', timeSpent: 4, category: 'Backend', type: 'task'},
    {id: 235, title: 'Create user registration UI', timeSpent: 8, category: 'Frontend', type: 'task'},
    {id: 237, title: 'User sign-in via Google UI', timeSpent: 3.5, category: 'Frontend', type: 'task'},
    {id: 238, title: 'User sign-in via Google API', timeSpent: 5, category: 'Backend', type: 'task'},
    {id: 241, title: 'Fix account linking', timeSpent: 5, category: 'Backend', type: 'bug'},
    {id: 250, title: 'Fix wrong time created on new record', timeSpent: 1, category: 'Backend', type: 'bug'},
    {id: 262, title: 'Fix sign-in failed messages', timeSpent: 2, category: 'Frontend', type: 'bug'},
];

const sum = tasks.reduce((accum, currentVal) => {
    if(currentVal.category === 'Frontend') {
        accum += currentVal.timeSpent
    }
    return accum
}, 0);

console.log(sum);

const sum2  = tasks.reduce((acc, currentVal)=>{
    if(currentVal.type === 'bug'){
        acc += currentVal.timeSpent
    }
    return acc
}, 0);
console.log(sum2);


const uiTasks = tasks.filter(task => task.title.includes('UI')).length;

console.log(uiTasks);

const frontBack = tasks.reduce((accum, task) => {
    if(task.category==='Frontend'){
        accum.Frontend++;
    }else if(task.category==='Backend'){
        accum.Backend++;
    }
    return accum;
}, {Frontend: 0, Backend: 0});

console.log(frontBack);


const moreFour = tasks.map(task => {
    if(task.timeSpent >= 4) {
        return {title: task.title, category: task.category}
    }
}).filter(task => {
    if(task !== undefined) {
        return task
    }
});

console.log(moreFour)

